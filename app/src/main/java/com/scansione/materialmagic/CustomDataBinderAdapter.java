package com.scansione.materialmagic;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by ajijul on 13/1/17.
 */

public class CustomDataBinderAdapter {

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).into(imageView);
        Log.e("CustomDataBinderAdapter", "URL : " + url);
    }
}
