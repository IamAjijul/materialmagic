package com.scansione.materialmagic;

import java.io.Serializable;

/**
 * Created by ajijul on 12/1/17.
 */
public class Sample implements Serializable {
    public int backColor;
    public String itemName;
    public String imgUrl;

    public Sample(int backColor, String itemName, String imgUrl) {
        this.backColor = backColor;
        this.itemName = itemName;
        this.imgUrl = imgUrl;
    }

    public int getBackColor() {
        return backColor;
    }

    public void setBackColor(int backColor) {
        this.backColor = backColor;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
